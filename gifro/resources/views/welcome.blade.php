@extends('layouts.master')
@section('master')

<div style="margin: 15px 45px 25px; display: flex; justify-content: end;">
    <a class="btn btn-primary" href="" role="button"> <i class="bi bi-box-arrow-in-left"></i>Voltar</a>
</div>

<div class="card auto-fit" style="margin: 20px 45px 25px;">
    <div class="card-header" style="background-color: rgb(49, 89, 228);">
        <h3 class="card-title" style="color: rgb(255, 255, 255); margin: 5px 5px 5px; font-size: 20px;">Cadastrar Tipo Documento</h3>
    </div>

    <form action="" method="POST"">
        @csrf

        <body class="d-flex flex-column">
            <section class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <label>Nome</label>
                        <input name="nome" type="text" class="form-control" placeholder="Digite o nome do tipo documento" value="">
                    </div>

                    <div class="col-md-6">
                        <label>Status</label>
                        <select name="status" class="form-select" value="" required>
                            <option selected disabled value="">Escolha um status</option>
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>
                    </div>

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Atenção!</h5>
                                </div>
                                <div class="modal-body">
                            
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="filtro-certificado"></div>
        </body>

        <footer class="card-footer" style="display: flex; justify-content: end;">
            <button href="/" type="submit" id="botaoEnviar" class="btn btn-success">
                <i>
                    <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z">
                        </path>
                    </svg>
                </i> Salvar
            </button>
        </footer>
    </form>
</div>

@endsection
